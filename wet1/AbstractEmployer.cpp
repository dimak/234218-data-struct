#include <stddef.h>

#include "AbstractEmployer.hpp"
#include "library1.h"

AbstractEmployer::AbstractEmployer() {
}

AbstractEmployer::~AbstractEmployer() {
	// TODO empty trees???
}

void AbstractEmployer::addEmployee(Employee* employee) {
	try {
		this->m_idTree.insert(employee);
		try {
			this->m_salaryTree.insert(employee);
		} catch (AVLTreeException &) {
			this->m_idTree.remove(employee);
		}
	} catch (AVLTreeException &) {
		throw Failure();
	}
}

void AbstractEmployer::removeEmployee(Employee* employee) {
	try {
		this->m_idTree.remove(employee);
		try {
			this->m_salaryTree.remove(employee);
		} catch (AVLTreeException &) {
			this->m_idTree.insert(employee);
		}
	} catch (AVLTreeException &e) {
		throw Failure();
	}
}

Employee* AbstractEmployer::getEmployeeById(unsigned int id) const {
	try {
		return this->m_idTree.findExactByParameter(EmployeeIdSearchParameter(id));
	} catch (AVLTreeException &) {
		return NULL;
	}
}




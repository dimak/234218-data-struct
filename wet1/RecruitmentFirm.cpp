/*
 * RecruitmentFirm.cpp
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#include "RecruitmentFirm.hpp"

RecruitmentFirm::RecruitmentFirm() {
	// TODO Auto-generated constructor stub

}

RecruitmentFirm::~RecruitmentFirm() {
	// TODO Auto-generated destructor stub
}

Employee* RecruitmentFirm::findEmployeeBySalary(unsigned int maxSalary) const {
	try {
		return this->m_salaryTree.findByParameter(EmployeeSalaryIdSearchParameter(maxSalary, -1));
	} catch (AVLTreeException &) {
		return NULL;
	}
}

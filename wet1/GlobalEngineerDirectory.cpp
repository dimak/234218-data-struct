/*
 * GlobalEngineerDirectory.cpp
 *
 *  Created on: 16  2014
 *      Author: Inbal
 */
#include "AVLTree.hpp"
#include "Employee.hpp"

class DeleteNode: public AbstractWalkParameter<Employee *> {
public:
	DeleteNode() {
	}
	virtual ~DeleteNode() {
	}
	virtual void apply(Employee * &e) {
		if (e->getFile())
			delete e->getFile();
		delete e;
	}
};

#include <stddef.h>

#include "GlobalEngineerDirectory.hpp"

GlobalEngineerDirectory::GlobalEngineerDirectory() {
}

GlobalEngineerDirectory::~GlobalEngineerDirectory() {

}

void GlobalEngineerDirectory::addEngineer(Employee* engineer) {
	try {
		this->m_idTree.insert(engineer);
	} catch (AVLTreeException &) {
		throw Failure();
	}
}

void GlobalEngineerDirectory::removeEngineer(Employee* engineer) {
	try {
		this->m_idTree.remove(engineer);
	} catch (AVLTreeException &) {
		throw Failure();
	}
}

void GlobalEngineerDirectory::clear() {
	DeleteNode param;
	this->m_idTree.walkNodes(param);
	this->m_idTree.clear();
}


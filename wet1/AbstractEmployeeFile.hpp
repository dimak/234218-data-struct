/*
 * AbstractEmployeeFile.h
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#ifndef ABSTRACTEMPLOYEEFILE_H_
#define ABSTRACTEMPLOYEEFILE_H_


class AbstractEmployeeFile {
public:
	AbstractEmployeeFile();
	virtual ~AbstractEmployeeFile();
};

#endif /* ABSTRACTEMPLOYEEFILE_H_ */

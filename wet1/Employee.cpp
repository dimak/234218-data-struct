/*
 * Employee.cpp
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#include <stddef.h>

#include "Employee.hpp"

Employee::Employee(unsigned int id, unsigned int salary)
	: m_id(id), m_salary(salary), m_file(NULL)
{

}

unsigned int Employee::getId() const {
	return m_id;
}

unsigned int Employee::getSalary() const {
	return m_salary;
}

void Employee::setSalary(unsigned int salary) {
	m_salary = salary;
}

AbstractEmployeeFile* Employee::getFile() const {
	return m_file;
}

void Employee::setFile(AbstractEmployeeFile* file) {
	m_file = file;
}

bool Employee::operator <(const Employee& other) {
	return this->getId() < other.getId();
}

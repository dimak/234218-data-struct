/*
 * DataStructure.cpp
 *
 *  Created on: Apr 9, 2014
 *      Author: dkuznets
 */

#include "DataStructure.h"

DataStructure::DataStructure(unsigned int numOfFirms) {
	this->numOfFirms = numOfFirms;
	this->m_firms = new HighTechFirm[numOfFirms];
}

DataStructure::~DataStructure() {
	this->m_dir.clear();
	delete[] this->GetHighTechFirms();
}

unsigned int DataStructure::GetNumOfFirms() {
	return this->numOfFirms;
}

HighTechFirm * DataStructure::GetHighTechFirms() {
	return this->m_firms;
}

RecruitmentFirm &DataStructure::GetRecruitmentFirm() {
	return this->recruitmentFirm;
}

void DataStructure::AddJobSearcher(unsigned int engineerID,
		unsigned int reqSalary) {
	Employee *newEmployee = new Employee(engineerID, reqSalary);
	try {
		this->m_dir.addEngineer(newEmployee);
		try {
			this->recruitmentFirm.addEmployee(newEmployee);
		} catch (Failure &) {
			this->m_dir.removeEngineer(newEmployee);
			throw;
		}
	} catch (Failure &) {
		delete newEmployee;
		throw;
	}
}

void DataStructure::RemoveJobSearcher(unsigned int engineerID) {
	//find employee
	Employee *employee = this->recruitmentFirm.getEmployeeById(engineerID);
	if (employee == NULL) {
		throw Failure();
	}

	//remove Employee
	this->recruitmentFirm.removeEmployee(employee);
	this->m_dir.removeEngineer(employee);

	delete employee;
}

void DataStructure::Hire(unsigned int companyID, unsigned int engineerID) {
	HighTechFirm &firm = this->GetFirm(companyID);
	Employee *employee = this->recruitmentFirm.getEmployeeById(engineerID);
	if (employee == NULL) {
		throw Failure();
	}
	firm.addEmployee(employee);
	this->recruitmentFirm.removeEmployee(employee);
}

void DataStructure::HireBySalary(unsigned int companyID,
		unsigned int salaryThd) {
	HighTechFirm &firm = this->GetFirm(companyID);
	Employee *employee = this->recruitmentFirm.findEmployeeBySalary(salaryThd);
	//search salary that less then salaryThd

	if (employee == NULL) {
		throw Failure();
	}
	//hire employee to companyID
	firm.addEmployee(employee);

	//remove employee from recruitment firm
	this->GetRecruitmentFirm().removeEmployee(employee);
}

void DataStructure::Bonus(unsigned int companyID, unsigned int engineerID,
		unsigned int bonus) {
	Employee *employee = this->GetFirm(companyID).getEmployeeById(engineerID);
	//search employee in companyID
	if (employee == NULL) {
		throw Failure();
	}
	//raise salary by bonus
	this->GetFirm(companyID).giveBonusToEmployee(employee, bonus);
}

void DataStructure::Fire(unsigned int companyID, unsigned int engineerID) {
	Employee *employee = this->GetFirm(companyID).getEmployeeById(engineerID);
	//search employee in companyID
	if (employee == NULL) {
		throw Failure();
	}
	this->GetRecruitmentFirm().addEmployee(employee);
	this->GetFirm(companyID).removeEmployee(employee);
}

void DataStructure::GetNumEmployed(unsigned int companyID, unsigned int* num) {
	if (num == NULL) {
		throw InvalidInput();
	}
	*num = this->GetFirm(companyID).size();
}
void DataStructure::HighestPaid(unsigned int companyID,
		unsigned int* engineerID) {
	if (engineerID == NULL) {
		throw InvalidInput();
	}
	if (this->GetFirm(companyID).size() == 0) {
		throw Failure();
	}

	*engineerID = this->GetFirm(companyID).getHighestPaid()->getId();
}

void DataStructure::CutBacks(unsigned int companyID, unsigned int salaryThd,
		unsigned int salaryDecrease) {
	//cutBacks
	this->GetFirm(companyID).performCutbacks(salaryDecrease, salaryThd);
}


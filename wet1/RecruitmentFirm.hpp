/*
 * RecruitmentFirm.h
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#ifndef RECRUITMENTFIRM_H_
#define RECRUITMENTFIRM_H_

#include "AbstractEmployer.hpp"

class RecruitmentFirm: public AbstractEmployer {
public:
	RecruitmentFirm();
	virtual ~RecruitmentFirm();

	Employee *findEmployeeBySalary(unsigned int maxSalary) const;
};

#endif /* RECRUITMENTFIRM_H_ */

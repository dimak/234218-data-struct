/*
 * library1.cpp
 *
 *  Created on: 9 ���� 2014
 *      Author: Inbal
 */
#include "library1.h"
#include "DataStructure.h"

#include <iostream>
using namespace std;

extern "C" {

#ifndef NULL
#define NULL   ((void *) 0)
#endif

#define CATCH_EXCEPTIONS(exp) \
	try { \
		(exp); \
	} catch (InvalidInput& e) { \
		return INVALID_INPUT; \
	} catch (std::bad_alloc& e) { \
		return ALLOCATION_ERROR; \
	} catch (Failure& e) { \
		return FAILURE; \
	} \
	return SUCCESS

void* Init(int K) {
	DataStructure *DS = NULL;
	try {
		DS = new DataStructure(K);
	} catch (...) {
		return NULL;
	}
	return (void*) DS;
}

StatusType AddJobSearcher(void* DS, int engineerID, int reqSalary) {
	if (DS == NULL || engineerID < 0 || reqSalary < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->AddJobSearcher(engineerID, reqSalary));
}

StatusType RemoveJobSearcher(void* DS, int engineerID) {
	if (DS == NULL || engineerID < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(((DataStructure* )DS)->RemoveJobSearcher(engineerID));
}

StatusType Hire(void* DS, int companyID, int engineerID) {
	if (DS == NULL || companyID < 0 || engineerID < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->Hire((unsigned int)companyID, (unsigned int)engineerID));
}

StatusType HireBySalary(void* DS, int companyID, int salaryThd) {
	if (DS == NULL || companyID < 0 || salaryThd < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->HireBySalary((unsigned int)companyID, (unsigned int)salaryThd));
}

StatusType Bonus(void* DS, int companyID, int engineerID, int bonus) {
	if (DS == NULL || companyID < 0 || engineerID < 0 || bonus < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->Bonus((unsigned int)companyID, (unsigned int)engineerID, (unsigned int)bonus));

}

StatusType Fire(void* DS, int companyID, int engineerID) {
	if (DS == NULL || companyID < 0 ||  engineerID < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->Fire((unsigned int)companyID, (unsigned int)engineerID));
}

StatusType GetNumEmployed(void* DS, int companyID, int* num) {
	if (DS == NULL || num == NULL || companyID < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->GetNumEmployed((unsigned int) companyID, (unsigned int *) num));
}

StatusType HighestPaid(void* DS, int companyID, int* engineerID) {
	if (DS == NULL || engineerID == NULL || companyID < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->HighestPaid((unsigned int) companyID, (unsigned int *) engineerID));
}

StatusType CutBacks(void* DS, int companyID, int salaryThd,
		int salaryDecrease) {
	if (DS == NULL || companyID < 0 || salaryThd < 0 || salaryDecrease < 0) {
		return INVALID_INPUT;
	}
	CATCH_EXCEPTIONS(
			((DataStructure* )DS)->CutBacks((unsigned int)companyID, (unsigned int)salaryThd, (unsigned int)salaryDecrease));
}

void Quit(void **DS) {
	try {
		delete ((DataStructure*) *DS);
		*DS = NULL;
	} catch (...) {
		cout << "fail..." << endl;
	}
}

}

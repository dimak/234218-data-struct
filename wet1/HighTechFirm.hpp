/*
 * HighTechFirm.h
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#ifndef HIGHTECHFIRM_H_
#define HIGHTECHFIRM_H_

#include "AbstractEmployer.hpp"
#include "List.hpp"

class HighTechFirm: public AbstractEmployer {
private:
	List<Employee *> m_salaryList;
public:
	HighTechFirm();
	virtual ~HighTechFirm();

	virtual void addEmployee(Employee *employee);
	virtual void removeEmployee(Employee *employee);

	unsigned int size() const;
	void performCutbacks(unsigned int payCut, unsigned int payThreshold);
	void giveBonusToEmployee(Employee *employee, unsigned int payIncrease);
	Employee *getHighestPaid();

};

#endif /* HIGHTECHFIRM_H_ */

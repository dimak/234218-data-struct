/*
 * DataStructure.h
 *
 *  Created on: 9th April 2014
 *      Author: Inbal
 */

#ifndef DATASTRUCTURE_H_
#define DATASTRUCTURE_H_

#include <exception>

#include "General.h"
#include "AVLTree.hpp"
#include "List.hpp"
#include "HighTechFirm.hpp"
#include "RecruitmentFirm.hpp"
#include "GlobalEngineerDirectory.hpp"

class DataStructure {
public:

	DataStructure(unsigned int numOfFirms);
	virtual ~DataStructure();
	void AddJobSearcher(unsigned int engineerID, unsigned int reqSalary);
	void RemoveJobSearcher(unsigned int engineerID);
	void Hire(unsigned int companyID, unsigned int engineerID);
	void HireBySalary(unsigned int companyID, unsigned int salaryThd);
	void Bonus(unsigned int companyID, unsigned int engineerID,
			unsigned int bonus);
	void Fire(unsigned int companyID, unsigned int engineerID);
	void GetNumEmployed(unsigned int companyID, unsigned int *num) ;
	void HighestPaid(unsigned int companyID, unsigned int* engineerID) ;
	void CutBacks(unsigned int companyID, unsigned int salaryThd,
			unsigned int salaryDecrease);
	unsigned int GetNumOfFirms();
	HighTechFirm * GetHighTechFirms();
	RecruitmentFirm &GetRecruitmentFirm();

private:

	unsigned int numOfFirms;
	HighTechFirm *m_firms;
	RecruitmentFirm recruitmentFirm;
	GlobalEngineerDirectory m_dir;

	HighTechFirm &GetFirm(unsigned int companyID) {
		if (companyID >= this->GetNumOfFirms()) {
			throw InvalidInput();
		}
		return this->m_firms[companyID];
	}
};

#endif /* DATASTRUCTURE_H_ */

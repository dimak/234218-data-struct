/*
 * HighTechFirm.cpp
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#include "HighTechFirm.hpp"
#include "HighTechEmployeeFile.hpp"
#include "DataStructure.h"

HighTechFirm::HighTechFirm() {
	// TODO Auto-generated constructor stub

}

HighTechFirm::~HighTechFirm() {
	// TODO Auto-generated destructor stub

}

unsigned int HighTechFirm::size() const {
	return this->m_salaryList.size();
}

void HighTechFirm::performCutbacks(unsigned int payCut,
		unsigned int payThreshold) {

	if (payCut > payThreshold) {
		throw InvalidInput();
	}

	if (!this->size())
		throw Failure();

	if (this->getHighestPaid()->getSalary() < payThreshold)
		return;

	List<Employee *>::Iterator splitItr = this->m_salaryList.begin();
	int internalOrderChanged = 0;

	while (splitItr.inBounds() && (*splitItr)->getSalary() < payThreshold) {
		splitItr++;
		internalOrderChanged = 1;
	}

	List<Employee *> needToReduceWages;
	this->m_salaryList.splitTail(needToReduceWages, splitItr);

	List<Employee *>::Iterator itr = needToReduceWages.begin();
	while (itr.inBounds()) {
		(*itr)->setSalary((*itr)->getSalary() - payCut);
		itr++;
	}

	if (!internalOrderChanged) {
		this->m_salaryList.clear();
		itr = needToReduceWages.begin();
		while (itr.inBounds()) {
			this->m_salaryList.append(*itr);

			delete (*itr)->getFile();
			(*itr)->setFile(new HighTechEmployeeFile(m_salaryList.end().advance(-1)));

			itr++;
		}

	} else {
		// Merge lists
		itr = this->m_salaryList.begin();
		List<Employee *>::Iterator secItr = needToReduceWages.begin();
		while (itr.inBounds() && secItr.inBounds()) {
			if (EmployeeSalaryCompare::compare((*secItr), (*itr))) {
				this->m_salaryList.insertBefore(itr, (*secItr));

				delete (*secItr)->getFile();
				(*secItr)->setFile(new HighTechEmployeeFile(itr.advance(-1)));

				itr++;
				secItr++;
			} else {
				itr++;
			}
		}
		while(secItr.inBounds()) {
			this->m_salaryList.append(*secItr);

			delete (*secItr)->getFile();
			(*secItr)->setFile(new HighTechEmployeeFile(m_salaryList.end().advance(-1)));

			secItr++;
		}

		// Rebuild tree
		this->m_salaryTree.reinitializeFromList(this->m_salaryList);
	}
}

void HighTechFirm::giveBonusToEmployee(Employee* employee,
		unsigned int payIncrease) {
	this->removeEmployee(employee);
	employee->setSalary(employee->getSalary() + payIncrease);
	this->addEmployee(employee);
}

void HighTechFirm::addEmployee(Employee* employee) {
	AbstractEmployer::addEmployee(employee);

	Employee *neighbor = NULL;
	try {
		neighbor = this->m_salaryTree.getNeighbor(employee);
	} catch (AVLTreeException &) {
		// Nothing
	}


	if (!neighbor) {
		this->m_salaryList.append(employee);
		employee->setFile(new HighTechEmployeeFile(this->m_salaryList.begin()));
	} else {
		HighTechEmployeeFile *neighborFile =
				dynamic_cast<HighTechEmployeeFile *>(neighbor->getFile());
		List<Employee *>::Iterator newItr = neighborFile->itr;
		if (EmployeeSalaryCompare::compare(neighbor, employee)) {
			this->m_salaryList.insertAfter(neighborFile->itr, employee);
			newItr++;
		} else if (EmployeeSalaryCompare::compare(employee, neighbor)) {
			this->m_salaryList.insertBefore(neighborFile->itr, employee);
			newItr--;
		}

		employee->setFile(new HighTechEmployeeFile(newItr));
	}
}

void HighTechFirm::removeEmployee(Employee* employee) {
	AbstractEmployer::removeEmployee(employee);
	HighTechEmployeeFile *file =
			dynamic_cast<HighTechEmployeeFile *>(employee->getFile());
	this->m_salaryList.removeAt(file->itr);
	delete file;
	employee->setFile(NULL);
}

Employee* HighTechFirm::getHighestPaid() {
	try {
		return *(this->m_salaryList.end().advance(-1));
	} catch (ListException &) {
		throw Failure();
	}
}

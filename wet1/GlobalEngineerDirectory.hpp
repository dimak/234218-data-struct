/*
 * GlobalEngineerDirectory.hpp
 *
 *  Created on: 16 ���� 2014
 *      Author: Inbal
 */

#ifndef GLOBALENGINEERDIRECTORY_HPP_
#define GLOBALENGINEERDIRECTORY_HPP_

#include "Employee.hpp"
#include "AVLTree.hpp"
#include "library1.h"

class GlobalEngineerDirectory {
private:
	AVLTree<Employee *, EmployeeIdCompare> m_idTree;

public:
	GlobalEngineerDirectory();
	virtual ~GlobalEngineerDirectory();
	void addEngineer(Employee *engineer);
	void removeEngineer(Employee *engineer);
	void clear();

};

#endif /* GLOBALENGINEERDIRECTORY_HPP_ */

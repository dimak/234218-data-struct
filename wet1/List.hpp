/*
 * List.hpp
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#ifndef LIST_HPP_
#define LIST_HPP_

#include <exception>
#include <stddef.h>
#include "General.h"

class ListException: public std::exception {
public:
	virtual const char *what() const throw () {
		return "List exception";
	}
};

class ListOutOfBoundsException: public ListException {
public:
	virtual const char *what() const throw () {
		return "List bounds violated";
	}
};

class ListEmptyException: public ListException {
public:
	virtual const char *what() const throw () {
		return "List is already empty";
	}
};

template<typename T>
class List {
protected:
	struct Node {
		Node *prev, *next;
		Node() :
				prev(NULL), next(NULL) {
		}
		virtual ~Node() {
		}
	};
	struct NodeWithData: public Node {
		T data;

		NodeWithData(T data) :
				Node(), data(data) {
		}
	};

public:
	class Iterator {
		friend class List<T> ;

		List<T> *m_list;
		Node *m_currentNode;

	public:
		Iterator(List<T> *list, List<T>::Node *at) :
				m_list(list), m_currentNode(at) {
		}

		inline bool hasPrev() const {
			return this->inBounds() && this->m_currentNode->prev;
		}

		inline bool hasNext() const {
			return this->inBounds() && this->m_currentNode->next;
		}

		inline bool inBounds() const {
			return this->m_currentNode != NULL
					&& dynamic_cast<NodeWithData *>(this->m_currentNode);
		}

		inline Iterator &advance(int steps) {
			if (steps > 0) {
				while (steps--)
					(*this)++;
			} else {
				while (steps++)
					(*this)--;
			}
			return *this;
		}

		inline T &operator*() {
			if (!this->inBounds())
				throw ListOutOfBoundsException();
			return dynamic_cast<NodeWithData *>(this->m_currentNode)->data;
		}

		inline bool operator==(const Iterator &other) const {
			return (this->m_list == other.m_list)
					&& (this->m_currentNode == other.m_currentNode);
		}

		inline bool operator!=(const Iterator &other) const {
			return !(*this == other);
		}

		inline List<T>::Iterator &operator++(int) {
			if (this->m_currentNode)
				this->m_currentNode = this->m_currentNode->next;
			return *this;
		}
		inline List<T>::Iterator &operator--(int) {
			if (this->m_currentNode)
				this->m_currentNode = this->m_currentNode->prev;
			return *this;
		}

	protected:
		void setCurrentNode(List<T>::NodeWithData *node) {
			this->m_currentNode = node;
		}
	};

protected:
	void initFromRange(Node* first, Node* last);
public:
	List();
	virtual ~List();

	List<T>::Iterator begin() {
		return Iterator(this, this->m_head->next);
	}

	List<T>::Iterator end() {
		return Iterator(this, this->m_tail);
	}

	unsigned int size() const {
		return this->m_size;
	}

	void insertBefore(List<T>::Iterator &iter, T elem);

	void insertAfter(List<T>::Iterator &iter, T elem);

	void removeAt(List<T>::Iterator &iter);

	void splitTail(List<T> &targetList, List<T>::Iterator &newHead);

	void append(T elem);

	void clear();

private:
	inline void nodeInsertBefore(Node *who, Node *where) {
		who->next = where;
		who->prev = where->prev;
		who->prev->next = who;
		where->prev = who;
	}

	inline void nodeInsertAfter(Node *who, Node *where) {
		who->prev = where;
		who->next = where->next;
		who->next->prev = who;
		where->next = who;
	}

	inline void nodeRemove(Node *who) {
		who->next->prev = who->prev;
		who->prev->next = who->next;
	}

	inline bool isTailNode(Node *node) {
		return node == &this->m_tail_marker;
	}

	void MergeLists(List<T>::Iterator listToMerge) {
		this->m_tail->next = listToMerge.m_list->m_head;
		listToMerge.m_list->m_head->prev = this->m_tail;
		this->m_tail = listToMerge.m_list->m_tail;
	}

	Node *m_head;
	Node *m_tail;

	Node m_head_marker;
	Node m_tail_marker;

	unsigned int m_size;
};

template<typename T>
inline List<T>::List() {
	this->m_size = 0;
	this->m_head = &this->m_head_marker;
	this->m_tail = &this->m_tail_marker;
	this->m_head->next = this->m_tail;
	this->m_tail->prev = this->m_head;
}

template<typename T>
inline List<T>::~List() {
	this->clear();
}

template<typename T>
inline void List<T>::removeAt(List<T>::Iterator &iter) {
	if (!iter.inBounds())
		throw ListOutOfBoundsException();
	this->nodeRemove(iter.m_currentNode);
	delete iter.m_currentNode;
	iter.m_currentNode = NULL;
	this->m_size--;
}

template<typename T>
inline void List<T>::insertBefore(List<T>::Iterator &iter, T elem) {
	if (iter.m_currentNode == this->m_head || !iter.m_currentNode)
		throw ListOutOfBoundsException();

	NodeWithData *newNode = new NodeWithData(elem);
	this->nodeInsertBefore(newNode, iter.m_currentNode);
	this->m_size++;
}

template<typename T>
inline void List<T>::insertAfter(List<T>::Iterator &iter, T elem) {
	if (iter.m_currentNode == this->m_tail || !iter.m_currentNode)
		throw ListOutOfBoundsException();

	NodeWithData *newNode = new NodeWithData(elem);
	this->nodeInsertAfter(newNode, iter.m_currentNode);
	this->m_size++;
}

template<typename T>
void List<T>::initFromRange(Node* first, Node* last) {
	this->clear();

	this->m_head->next = first;
	first->prev = this->m_head;

	this->m_tail->prev = last;
	last->next = this->m_tail;

	Node *p = first;
	this->m_size = 1;
	while (p != last) {
		this->m_size++;
		p = p->next;
	}
}

template<typename T>
inline void List<T>::append(T elem) {
	NodeWithData *newNode = new NodeWithData(elem);
	this->nodeInsertBefore(newNode, this->m_tail);
	this->m_size++;
}

template<typename T>
void List<T>::splitTail(List<T> &targetList, List<T>::Iterator &newHead) {
	// split this list right before newHead, return new list with newHead as head
	// this list goes up to this[newHead - 1]
	Node *last = this->m_tail->prev;
	this->m_tail->prev = newHead.m_currentNode->prev;
	newHead.m_currentNode->prev->next = this->m_tail;
	targetList.initFromRange(newHead.m_currentNode, last);
	this->m_size -= targetList.size();
}

template<typename T>
inline void List<T>::clear() {
	Node *current = this->m_head->next;

	while (current != this->m_tail) {
		current = current->next;
		delete current->prev;
	}

	this->m_size = 0;
	this->m_head = &this->m_head_marker;
	this->m_tail = &this->m_tail_marker;
	this->m_head->next = this->m_tail;
	this->m_tail->prev = this->m_head;
}

#endif /* LIST_HPP_ */

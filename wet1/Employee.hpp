#ifndef _EMPLOYEE_H
#define _EMPLOYEE_H

#include "AbstractEmployeeFile.hpp"

class Employee
{
private:
    unsigned int m_id;
    unsigned int m_salary;
    AbstractEmployeeFile *m_file;

public:
    Employee(unsigned int id, unsigned int salary);
    virtual ~Employee() { }
	unsigned int getId() const;
	unsigned int getSalary() const;
	void setSalary(unsigned int salary);

	bool operator < (const Employee &other);
	AbstractEmployeeFile* getFile() const;
	void setFile(AbstractEmployeeFile* file);
};

#endif /* _EMPLOYEE_H */

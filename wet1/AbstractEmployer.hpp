#ifndef _ABSTRACT_EMPLOYER_HPP
#define _ABSTRACT_EMPLOYER_HPP

#include "Employee.hpp"
#include "AVLTree.hpp"
#include "library1.h"


class EmployeeSalaryCompare {
public:
	static bool compare(Employee *e1, Employee *e2) {
		if (e1->getSalary() == e2->getSalary())
			return EmployeeIdCompare::compare(e1, e2);
		return e1->getSalary() < e2->getSalary();
	}
};

class EmployeeIdSearchParameter : public AbstractSearchParameter<Employee *> {
private:
	unsigned int m_id;

public:
	EmployeeIdSearchParameter(unsigned int id) : m_id(id) { }
	virtual ~EmployeeIdSearchParameter() { }

	virtual int compare(Employee *employee) const {
		if (!employee)
			return 0;

		return this->m_id - employee->getId();
	}
};

class EmployeeSalaryIdSearchParameter : public AbstractSearchParameter<Employee *> {
private:
	unsigned int m_salary;
	unsigned int m_id;

public:
	EmployeeSalaryIdSearchParameter(unsigned int salary, unsigned int id) : m_salary(salary), m_id(id) {}
	virtual ~EmployeeSalaryIdSearchParameter() {}

	virtual int compare(Employee *employee) const {
		int res;

		if (!employee)
			return 0;

		res = this->m_salary - employee->getSalary();
		if (!res) {
			if (this->m_id > employee->getId())
				return 1;
			else if (this->m_id == employee->getId())
				return 0;
			else return -1;
		}
		return res;
	}
};

class AbstractEmployer {
protected:
	AVLTree<Employee *, EmployeeIdCompare> m_idTree;
	AVLTree<Employee *, EmployeeSalaryCompare> m_salaryTree;

public:
    AbstractEmployer();
    virtual ~AbstractEmployer();

	virtual void addEmployee(Employee *employee);
	virtual void removeEmployee(Employee *employee);

	Employee *getEmployeeById(unsigned int id) const;

};

#endif /* _ABSTRACT_EMPLOYER_HPP */

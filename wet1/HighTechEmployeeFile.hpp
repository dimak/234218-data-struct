/*
 * HighTechEmployeeFile.hpp
 *
 *  Created on: Apr 8, 2014
 *      Author: dkuznets
 */

#ifndef HIGHTECHEMPLOYEEFILE_HPP_
#define HIGHTECHEMPLOYEEFILE_HPP_

#include "AbstractEmployeeFile.hpp"
#include "Employee.hpp"
#include "List.hpp"

class HighTechEmployeeFile: public AbstractEmployeeFile {
public:
	List<Employee *>::Iterator itr;

	HighTechEmployeeFile(List<Employee *>::Iterator itr);
	virtual ~HighTechEmployeeFile();
};

#endif /* HIGHTECHEMPLOYEEFILE_HPP_ */

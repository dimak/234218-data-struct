#ifndef UNIONFIND_HPP_
#define UNIONFIND_HPP_

#include <cstddef>
#include <exception>

#include "Pair.hpp"
#include "Candidate.hpp"

class UnionFindException : public std::exception { };
class UnionFindInvalidElementId : public UnionFindException { };
class UnionFindInvalidGroupId : public UnionFindException { };
class UnionFindSameGroupMerge : public UnionFindException { };

class ReverseTreeNode {
public:
	ReverseTreeNode(unsigned int id);
	virtual ~ReverseTreeNode();
	virtual void setParent(ReverseTreeNode *new_parent);
	unsigned int getId() const;
	virtual ReverseTreeNode *getParent() const;

protected:
	unsigned int m_id;
	ReverseTreeNode *m_parent;
};

class UnionFind {
public:
	UnionFind(size_t size);
	void init();
	virtual ~UnionFind();
	virtual unsigned int find(unsigned int id);
	virtual void merge(unsigned int group_id1, unsigned int group_id2);
	size_t groupSize(unsigned int group_id) const;

public:
	virtual ReverseTreeNode *allocTreeNode(unsigned int id) const;

protected:
	unsigned int m_size;
	ReverseTreeNode **m_elements;
	ReverseTreeNode **m_groups;
	int *m_group_sizes;

};

class TopReverseTreeNode: public ReverseTreeNode {
public:
	TopReverseTreeNode(unsigned int id);
	void setCandidate(const Candidate *candidate);
	void setParent(ReverseTreeNode *new_parent);
	void update();
	const Candidate *getFirst() const;

private:
	const Candidate *m_first;
	const Candidate *m_candidate;
};

class CandidateUnionFind: public UnionFind {
public:
	CandidateUnionFind(Candidate **candidates, size_t count);
	virtual ~CandidateUnionFind();

	unsigned int find(const Candidate *candidate);

	void merge(unsigned int group_id1, unsigned int group_id2);

	void updateScore(const Candidate *candidate);
	bool isTopCandidate(const Candidate *candidate);
	const Candidate *getLeader(const Candidate *candidate);

//protected:
	virtual ReverseTreeNode *allocTreeNode(unsigned int id) const;
};
#endif /* UNIONFIND_HPP_ */

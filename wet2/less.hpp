/*
 * less.hpp
 *
 *  Created on: Apr 8, 2014
 *      Author: dkuznets
 */

#ifndef LESS_HPP_
#define LESS_HPP_

template <typename T>
class less {
public:
	virtual ~less() { }
	static bool compare(T t1, T t2) {
		return t1 < t2;
	}
};


#endif /* LESS_HPP_ */

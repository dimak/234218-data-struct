/*
 * Candidate.cpp
 *
 *  Created on: 2 ���� 2014
 *      Author: Inbal
 */

#include "Candidate.hpp"

Candidate::~Candidate() {
}

Candidate::Candidate(unsigned int id) : m_id(id), m_votes(0) {
}

unsigned int Candidate::getId() const {
	return this->m_id;
}

unsigned int Candidate::getVotes() const {
	return this->m_votes;
}

void Candidate::addVote() {
	this->m_votes++;
}

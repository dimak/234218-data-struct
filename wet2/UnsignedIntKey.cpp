#include "UnsignedIntKey.hpp"

UnsignedIntKey::UnsignedIntKey(unsigned int key) : m_key(key) {
}

UnsignedIntKey::~UnsignedIntKey() {
}

unsigned int UnsignedIntKey::hash() const {
	return this->m_key;
}

bool UnsignedIntKey::compareTo(const IComparable& other) const {
	const UnsignedIntKey *castedOther = dynamic_cast<const UnsignedIntKey *>(&other);

	if (castedOther == NULL)
		return false;
	return castedOther->m_key == this->m_key;
}

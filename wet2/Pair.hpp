#ifndef PAIR_HPP_
#define PAIR_HPP_

template <typename TFirst, typename TSecond>
class Pair {
public:
	TFirst first;
	TSecond second;

	Pair(TFirst first, TSecond second) : first(first), second(second) {}
};


#endif /* PAIR_HPP_ */

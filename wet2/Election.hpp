#ifndef ELECTION_H_
#define ELECTION_H_

#include <cstddef>
#include <exception>
#include "Candidate.hpp"
#include "AVLTree.hpp"
#include "DHash.hpp"
#include "UnsignedIntKey.hpp"
#include "UnionFind.hpp"

class ElectionException : public std::exception {};
class ElectionDoubleVoteAttempt : public ElectionException {};
class ElectionInvalidCandidateId : public ElectionException {};
class ElectionIneligibleCandidate : public ElectionException {};
class ElectionCandidatesFromSameCamp : public ElectionException {};

class Election {
public:
	Election(size_t candidate_count);
	virtual ~Election();

	void registerVote(unsigned int voter_id, unsigned int candidate_id);
	void signAgreement(unsigned int candidate_a_id, unsigned int candidate_b_id);
	unsigned int getCampLeader(unsigned int candidate_id);
	void getCurrentRanking(int results[][2]);

private:
	Candidate *getCandidate(unsigned int id) const;

	size_t m_candidate_count;
	Candidate **m_candidate_directory;
	bool *m_candidate_in_scoreboard;
	DHash<UnsignedIntKey, bool> *m_ballot_box;
	AVLTree<Candidate *, CandidateCompare> *m_candidate_scoreboard;
	CandidateUnionFind *m_camp_tracker;
};

#endif /* ELECTION_H_ */

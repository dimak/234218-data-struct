/*
 * UnsignedIntKey.h
 *
 *  Created on: Jun 2, 2014
 *      Author: dkuznets
 */

#ifndef UNSIGNEDINTKEY_H_
#define UNSIGNEDINTKEY_H_

#include "Hash.hpp"

class UnsignedIntKey : public AbstractKeyType {
public:
	UnsignedIntKey(unsigned int key);
	virtual ~UnsignedIntKey();
	virtual unsigned int hash() const;
	virtual bool compareTo(const IComparable &other) const;

private:
	unsigned int m_key;
};

#endif /* UNSIGNEDINTKEY_H_ */

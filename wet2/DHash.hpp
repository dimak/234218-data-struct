#ifndef DHASH_HPP_
#define DHASH_HPP_

#include "Hash.hpp"

template <typename TKeyType, typename TValueType>
class DHash : public Hash<TKeyType, TValueType> {
	typedef Hash<TKeyType, TValueType> super;

public:
	DHash(size_t initial_size = 8);
	virtual ~DHash();

	virtual void addKey(const TKeyType &key, const TValueType &value);
	virtual void removeKey(const TKeyType &key);

protected:
	void resize(size_t new_size);

	bool m_do_resize;
};

template <typename TKeyType, typename TValueType>
DHash<TKeyType, TValueType>::DHash(size_t initial_size)
	: Hash<TKeyType, TValueType>(initial_size), m_do_resize(false) {

}


template <typename TKeyType, typename TValueType>
DHash<TKeyType, TValueType>::~DHash() {

}

template <typename TKeyType, typename TValueType>
void DHash<TKeyType, TValueType>::addKey(const TKeyType &key, const TValueType &value) {
	super::addKey(key, value);

	if ((this->m_size * 2) > this->m_max_size) {
		this->m_do_resize = true;
		this->resize(this->m_max_size * 2);
	}
}

template <typename TKeyType, typename TValueType>
void DHash<TKeyType, TValueType>::removeKey(const TKeyType &key) {
	super::removeKey(key);

	if (this->m_do_resize && ((this->m_size * 4) < this->m_max_size)) {
		this->resize(this->m_max_size / 2);
	}
}

template <typename TKeyType, typename TValueType>
void DHash<TKeyType, TValueType>::resize(size_t new_size) {
	size_t old_size = this->m_max_size;
	List<Pair<TKeyType, TValueType> > **old_hash = this->m_hash;

	this->m_hash = new List<Pair<TKeyType, TValueType> >*[new_size];
	for (unsigned int i = 0; i < new_size; i++) {
		this->m_hash[i] = NULL;
	}
	this->m_max_size = new_size;
	this->m_size = 0;

	for (unsigned int i = 0; i < old_size; i++) {
		List<Pair<TKeyType, TValueType> > *list = old_hash[i];
		if (list == NULL) {
			continue;
		}

		typename List<Pair<TKeyType, TValueType> >::Iterator itr = list->begin();
		while (itr.inBounds()) {
			super::addKey((*itr).first, (*itr).second);
			itr++;
		}

		delete list;
	}

	delete [] old_hash;
}

#endif /* DHASH_HPP_ */

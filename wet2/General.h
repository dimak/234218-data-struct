/*
 * General.h
 *
 *  Created on: Apr 9, 2014
 *      Author: dkuznets
 */

#ifndef GENERAL_H_
#define GENERAL_H_


#include <exception>

class NotImplementedException : public std::exception {
public:
	virtual const char *what() const throw() { return "Not implemented"; }
};

class InvalidInput: public std::exception {
	virtual const char* what() const throw () {
		return "Invalid input";
	}
};
class Failure: public std::exception {
	virtual const char* what() const throw () {
		return "Failure";
	}
};

#endif /* GENERAL_H_ */

#include "Election.hpp"

Election::Election(size_t candidate_count) :
		m_candidate_count(candidate_count) {
	this->m_candidate_directory = new Candidate*[this->m_candidate_count];
	this->m_candidate_in_scoreboard = new bool[this->m_candidate_count];
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		this->m_candidate_directory[i] = new Candidate(i);
		this->m_candidate_in_scoreboard[i] = false;
	}
	this->m_ballot_box = new DHash<UnsignedIntKey, bool>(8);
	this->m_candidate_scoreboard = new AVLTree<Candidate *, CandidateCompare>();
	this->m_camp_tracker = new CandidateUnionFind(this->m_candidate_directory,
			this->m_candidate_count);
}

Election::~Election() {
	delete this->m_camp_tracker;
	delete[] this->m_candidate_in_scoreboard;
	delete this->m_ballot_box;
	delete this->m_candidate_scoreboard;
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		delete this->m_candidate_directory[i];
	}
	delete[] this->m_candidate_directory;
}

void Election::registerVote(unsigned int voter_id, unsigned int candidate_id) {
	Candidate *candidate = this->getCandidate(candidate_id);

	if (this->m_ballot_box->hasKey(voter_id))
		throw ElectionDoubleVoteAttempt();

	this->m_ballot_box->addKey(voter_id, true);
	if (this->m_candidate_in_scoreboard[candidate_id])
		this->m_candidate_scoreboard->remove(candidate);
	candidate->addVote();
	this->m_candidate_scoreboard->insert(candidate);
	this->m_candidate_in_scoreboard[candidate_id] = true;
	this->m_camp_tracker->updateScore(candidate);
}

void Election::signAgreement(unsigned int candidate_a_id,
		unsigned int candidate_b_id) {
	Candidate *candidate_a = this->getCandidate(candidate_a_id);
	Candidate *candidate_b = this->getCandidate(candidate_b_id);

	unsigned int group_a = this->m_camp_tracker->find(candidate_a);
	unsigned int group_b = this->m_camp_tracker->find(candidate_b);

	if (!(this->m_camp_tracker->getLeader(candidate_a)->getVotes()
			== candidate_a->getVotes())
			|| !(this->m_camp_tracker->getLeader(candidate_b)->getVotes()
					== candidate_b->getVotes())) {
		throw ElectionIneligibleCandidate();
	}

	if (group_a == group_b) {
		throw ElectionCandidatesFromSameCamp();
	}

	this->m_camp_tracker->merge(group_a, group_b);
}

unsigned int Election::getCampLeader(unsigned int candidate_id) {
	return this->m_camp_tracker->getLeader(this->getCandidate(candidate_id))->getId();
}

class TreeToArrayWalkParameter: public AbstractWalkParameter<Candidate *> {
public:
	TreeToArrayWalkParameter(Candidate **array, size_t size);
	virtual ~TreeToArrayWalkParameter() {
	}

	size_t size() const;

	virtual void apply(Candidate *&candidate);

private:
	size_t m_array_size;
	size_t m_current_size;
	Candidate **m_array;
};

TreeToArrayWalkParameter::TreeToArrayWalkParameter(Candidate **array,
		size_t size) :
		m_array_size(size), m_current_size(0), m_array(array) {
}

size_t TreeToArrayWalkParameter::size() const {
	return this->m_current_size;
}

void TreeToArrayWalkParameter::apply(Candidate *&candidate) {
	if (this->m_current_size >= this->m_array_size)
		return;
	this->m_array[this->m_current_size] = candidate;
	this->m_current_size++;
}

void Election::getCurrentRanking(int results[][2]) {
	// Get sorted array of candidates:
	Candidate *sorted_candidates[this->m_candidate_count];
	TreeToArrayWalkParameter walk_param(sorted_candidates,
			this->m_candidate_count);

	this->m_candidate_scoreboard->walkNodes(walk_param);

	//reverse sorted_candidates
	Candidate *reverse_candidates[this->m_candidate_count];
	for (unsigned int j = 0; j < walk_param.size(); j++) {
		int current_index = (walk_param.size() - j - 1);
		reverse_candidates[j] = sorted_candidates[current_index];
	}

	// Add all the non in-tree candidates, reverse order.
	unsigned int array_size = walk_param.size();
	for (unsigned int i = this->m_candidate_count - 1; i != (unsigned int) -1;
			i--) {
		if (this->m_candidate_in_scoreboard[i]) {
			continue;
		}
		reverse_candidates[array_size] = this->m_candidate_directory[i];
		array_size++;
	}

	// Find camp of each candidate
	unsigned int sorted_camps[this->m_candidate_count];
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		sorted_camps[i] = this->m_camp_tracker->find(reverse_candidates[i]);
	}

	// Find group size of each camp
	unsigned int camp_sizes[this->m_candidate_count];
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		camp_sizes[i] = this->m_camp_tracker->groupSize(i);
	}

	// Find the order in which the camps appear
	// (maps camp_id => order of appearance in sorted camps)
	unsigned int camp_order[this->m_candidate_count];
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		camp_order[i] = (unsigned int) -1;
	}

	unsigned int camps_last_place = 0; // always less-equal to #candidates
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		if (camp_order[sorted_camps[i]] < this->m_candidate_count)
			continue;
		camp_order[sorted_camps[i]] = camps_last_place;
		camps_last_place++;
	}

	// Calculate where each camp should start in the sorted array

	// Zero the offsets
	unsigned int ordered_camp_offsets[this->m_candidate_count + 1];
	for (unsigned int i = 0; i < this->m_candidate_count + 1; i++) {
		ordered_camp_offsets[i] = 0;
	}

	// Each camp's size, goes to next camps location.
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		if (camp_order[i] > this->m_candidate_count)
			continue;
		ordered_camp_offsets[1 + camp_order[i]] = camp_sizes[i];
	}

	// Summing all prior fields : [i] = [0] + [1] + ... + [i]
	for (unsigned int i = 1; i < this->m_candidate_count; i++) {
		ordered_camp_offsets[i] += ordered_camp_offsets[i - 1];
	}

	// Go over the initial sorted candidates, and write each candidate
	// to the offset of his group, while incrementing the offset.
	for (unsigned int i = 0; i < this->m_candidate_count; i++) {
		unsigned int current_camp = sorted_camps[i];
		unsigned int current_camp_order = camp_order[current_camp];
		results[ordered_camp_offsets[current_camp_order]][0] =
				reverse_candidates[i]->getId();
		results[ordered_camp_offsets[current_camp_order]][1] =
				reverse_candidates[i]->getVotes();
		ordered_camp_offsets[current_camp_order]++;
	}
}

Candidate *Election::getCandidate(unsigned int id) const {
	if (id >= this->m_candidate_count)
		throw ElectionInvalidCandidateId();
	return this->m_candidate_directory[id];
}

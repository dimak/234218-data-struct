/****************************************************************************/
/*                                                                          */
/* This file contains the interface functions                               */
/* you should use for the wet ex 2                                          */ 
/*                                                                          */ 
/****************************************************************************/

/****************************************************************************/
/*                                                                          */ 
/* File Name : library2.h                                                   */ 
/*                                                                          */ 
/****************************************************************************/

#ifndef _234218_WET2_
#define _234218_WET2_


#ifdef __cplusplus
extern "C" {
#endif




/* Return Values
 * ----------------------------------- */
typedef enum {
	SUCCESS = 0,
	FAILURE = -1,
	ALLOCATION_ERROR = -2,
	INVALID_INPUT = -3

} StatusType;



/* Required Interface for the Data Structure
 * -----------------------------------------*/



/* Description:   Initializes the election with n candidates.
 * Input:         DS - A pointer to the data structure.
 *                n - Number of candidates in the election.
 * Output:        None.
 * Return Values: A pointer to a new instance of the data structure - as a void* pointer.
 */
void*       Init(int n);







/* Description:   A candidate receives a vote by a voter.
 * Input:         DS - A pointer to the data structure.
 *                voterID - The ID of the voter.
 *                candidate - The ID of the candidate.
 * Output:        None.
 * Return Values: ALLOCATION_ERROR - In case of an allocation error.
 *                INVALID_INPUT - If DS==NULL, voterID<0 or candidate is not an illegal candidate number.
 *                FAILURE - If a voter with an ID of voterID has already casted a vote, or in case of any other error.
 *                SUCCESS - Otherwise.
 */
StatusType  Vote(void* DS, int voterID, int candidate);







/* Description:   Signs an agreement between the camps of candidate1,candidate2
 *                only if both candidates have the maximum votes in their camps.
 *                Notice that if there's a camp with more than one candidate that
 *                has the maximal number of votes, all of them can sign agreements.
 * Input:         DS - A pointer to the data structure.
 *                candidate1 - The identifier of the 1st candidate.
 *                candidate2 - The identifier of the 2nd candidate.
 * Output:        None.
 * Return Values: ALLOCATION_ERROR - In case of an allocation error.
 *                INVALID_INPUT - If DS==NULL or either candidate1 or candidate2 are illegal candidate numbers.
 *                FAILURE - If either candidate is not a camp leader, both candidates belong to the same camp, or in case of any other error.
 *                SUCCESS - Otherwise.
 */
StatusType  SignAgreement(void* DS, int candidate1, int candidate2);







/* Description:   Returns the leader of the camp in which candidate belongs.
 *                The leader is the candidate which has the maximal number of
 *                votes in the camp. In case more than one of the candidates in
 *                the camp have the maximal number of votes, the one with the
 *                largest index will be chosen.
 * Input:         DS - A pointer to the data structure.
 *                candidate - The identifier of the candidate.
 * Output:        leader - The identifier of the camp leader.
 * Return Values: INVALID_INPUT - If DS==NULL, candidate is not an illegal candidate number or leader==NULL.
 *                FAILURE - In case of an error.
 *                SUCCESS - Otherwise.
 */
StatusType  CampLeader(void* DS, int candidate, int* leader);







/* Description:   Returns an array of the current ranking of the candidates, as
 *                defined in wet2Spring2014.docx.
 * Input:         DS - A pointer to the data structure.
 * Output:        results - An array of size nx2 where the ranking will be written.
 * Return Values: ALLOCATION_ERROR - In case of an allocation error.
 *                INVALID_INPUT - If DS==NULL or results==NULL.
 *                FAILURE - In case of an error.
 *                SUCCESS - Otherwise.
 */
StatusType  CurrentRanking(void* DS, int results[][2]);







/* Description:   Quits and deletes the database.
 *                The variable pointed by DS should be set to NULL.
 * Input:         DS - A pointer to the data structure.
 * Output:        None.
 * Return Values: None.
 */
void        Quit(void** DS);





#ifdef __cplusplus
}
#endif

#endif    /*_234218_WET2_ */ 


/*
 * Candidate.hpp
 *
 *  Created on: Jun 3, 2014
 *      Author: dkuznets
 */

#ifndef CANDIDATE_HPP_
#define CANDIDATE_HPP_


class Candidate {
public:
	virtual ~Candidate();
	Candidate(unsigned int id);

	unsigned int getId() const;
	unsigned int getVotes() const;

	void addVote();

private:
	unsigned int m_id;
	unsigned int m_votes;
};

class CandidateCompare {
public:
	virtual ~CandidateCompare() { }
	static bool compare(const Candidate *c1, const Candidate *c2) {
		if (c1->getVotes() == c2->getVotes())
			return c1->getId() < c2->getId();
		return c1->getVotes() < c2->getVotes();
	}
};

#endif /* CANDIDATE_HPP_ */

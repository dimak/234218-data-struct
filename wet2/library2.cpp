#include "library2.h"

#include <cstddef>
#include <new>

#include "Election.hpp"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void *Init(int n) {
	if (n < 0) {
		return NULL;
	}
	return new Election((unsigned int) n);
}

StatusType Vote(void* DS, int voterID, int candidate) {
	if (DS == NULL || voterID < 0 || candidate < 0) {
		return INVALID_INPUT;
	}

	try {
		Election *election = (Election *) DS;
		election->registerVote((unsigned int) voterID, (unsigned int) candidate);
		return SUCCESS;
	} catch (std::bad_alloc &e) {
		return ALLOCATION_ERROR;
	} catch (ElectionInvalidCandidateId &e) {
		return INVALID_INPUT;
	} catch (ElectionException &e) {
		return FAILURE;
	}
}

StatusType SignAgreement(void* DS, int candidate1, int candidate2) {
	if (DS == NULL || candidate1 < 0 || candidate2 < 0) {
		return INVALID_INPUT;
	}

	try {
		Election *election = (Election *) DS;
		election->signAgreement((unsigned int) candidate1, (unsigned int) candidate2);
		return SUCCESS;
	} catch (std::bad_alloc &e) {
		return ALLOCATION_ERROR;
	} catch (ElectionInvalidCandidateId &e) {
		return INVALID_INPUT;
	} catch (ElectionException &e) {
		return FAILURE;
	}
}


StatusType CampLeader(void* DS, int candidate, int* leader) {
	if (DS == NULL || leader == NULL || candidate < 0) {
		return INVALID_INPUT;
	}

	try {
		Election *election = (Election *) DS;
		*leader = election->getCampLeader((unsigned int) candidate);
		return SUCCESS;
	} catch (std::bad_alloc &e) {
		return ALLOCATION_ERROR;
	} catch (ElectionInvalidCandidateId &e) {
		return INVALID_INPUT;
	} catch (ElectionException &e) {
		return FAILURE;
	}
}

StatusType CurrentRanking(void* DS, int results[][2]) {
	if (DS == NULL || results == NULL) {
		return INVALID_INPUT;
	}

	try {
		Election *election = (Election *) DS;
		election->getCurrentRanking(results);
		return SUCCESS;
	} catch (std::bad_alloc &e) {
		return ALLOCATION_ERROR;
	} catch (ElectionException &e) {
		return FAILURE;
	}
}

void Quit(void** DS) {
	Election *election = (Election *) *DS;
	delete election;
	*DS = NULL;
}

#ifdef __cplusplus
}
#endif // __cplusplus

#include <stddef.h>

#include "UnionFind.hpp"

ReverseTreeNode::ReverseTreeNode(unsigned int id) {
	this->m_id = id;
	this->m_parent = NULL;
}
ReverseTreeNode::~ReverseTreeNode() {}

void ReverseTreeNode::setParent(ReverseTreeNode *new_parent) {
	this->m_parent = new_parent;
}

unsigned int ReverseTreeNode::getId() const {
	return this->m_id;
}

ReverseTreeNode *ReverseTreeNode::getParent() const {
	return this->m_parent;
}

UnionFind::UnionFind(size_t size) : m_size(size), m_elements(NULL), m_groups(NULL), m_group_sizes(NULL) {
}

void UnionFind::init() {
	this->m_elements = new ReverseTreeNode*[this->m_size];
	this->m_groups = new ReverseTreeNode*[this->m_size];
	this->m_group_sizes = new int[this->m_size];
	for (unsigned int i = 0; i < this->m_size; i++) {
		this->m_elements[i] = this->allocTreeNode(i);
		this->m_groups[i] = this->m_elements[i];
		this->m_group_sizes[i] = 1;
	}
}

UnionFind::~UnionFind() {
	for (unsigned int i = 0; i < this->m_size; i++) {
		delete this->m_elements[i];
	}
	delete [] this->m_elements;
	delete [] this->m_groups;
	delete [] this->m_group_sizes;
}

unsigned int UnionFind::find(unsigned int id) {
	if (id >= this->m_size) {
		throw UnionFindInvalidElementId();
	}
	ReverseTreeNode *current_node = this->m_elements[id];
	ReverseTreeNode *top_node;

	while (current_node->getParent() != NULL) {
		current_node = current_node->getParent();
	}
	top_node = current_node;

	current_node = this->m_elements[id];
	while (current_node->getParent() != NULL) {
		ReverseTreeNode * old_parent = current_node->getParent();
		current_node->setParent(top_node);
		current_node = old_parent;
	}

	return top_node->getId();
}

void UnionFind::merge(unsigned int group_id1, unsigned int group_id2) {
	if ((group_id1 >= this->m_size || this->m_group_sizes[group_id1] == 0) ||
		(group_id2 >= this->m_size || this->m_group_sizes[group_id2] == 0)) {
		throw UnionFindInvalidGroupId();
	}

	if (group_id1 == group_id2) {
		throw UnionFindSameGroupMerge();
	}

	// Group1 is always smaller than group2
	if (this->m_group_sizes[group_id1] > this->m_group_sizes[group_id2]) {
		unsigned int tmp;
		tmp = group_id1;
		group_id1 = group_id2;
		group_id2 = tmp;
	}

	ReverseTreeNode *group1_root = this->m_groups[group_id1];
	ReverseTreeNode *group2_root = this->m_groups[group_id2];

	group1_root->setParent(group2_root);
	this->m_group_sizes[group_id2] += this->m_group_sizes[group_id1];
	this->m_group_sizes[group_id1] = 0;
	this->m_groups[group_id1] = NULL;
}

size_t UnionFind::groupSize(unsigned int group_id) const {
	return this->m_group_sizes[group_id];
}

ReverseTreeNode *UnionFind::allocTreeNode(unsigned int id) const {
	return new ReverseTreeNode(id);
}

TopReverseTreeNode::TopReverseTreeNode(unsigned int id) : ReverseTreeNode(id) {
	this->m_first = NULL;
	this->m_candidate = NULL;
}

void TopReverseTreeNode::setCandidate(const Candidate *candidate) {
	if (this->m_first == NULL)
		this->m_first = this->m_candidate = candidate;
}

void TopReverseTreeNode::setParent(ReverseTreeNode *new_parent) {
	this->m_parent = new_parent;
	TopReverseTreeNode *parent = dynamic_cast<TopReverseTreeNode *>(this->m_parent);

	if (CandidateCompare::compare(parent->m_first, this->m_first)) {
		parent->m_first = this->m_first;
	}
}

void TopReverseTreeNode::update() {
	if (this->m_first == this->m_candidate) {
		// Do nothing.
	} else if (CandidateCompare::compare(this->m_first, this->m_candidate)) {
		this->m_first = this->m_candidate;
	}
}

const Candidate *TopReverseTreeNode::getFirst() const {
	return this->m_first;
}

CandidateUnionFind::CandidateUnionFind(Candidate **candidates, size_t count)
	: UnionFind(count) {
	this->init();
	for (unsigned int i = 0; i < this->m_size; i++) {
		TopReverseTreeNode *node = dynamic_cast<TopReverseTreeNode *>(this->m_elements[i]);
		node->setCandidate(candidates[i]);
	}
}

CandidateUnionFind::~CandidateUnionFind() {
}

unsigned int CandidateUnionFind::find(const Candidate *candidate) {
		return UnionFind::find(candidate->getId());
}

void CandidateUnionFind::merge(unsigned int group_id1, unsigned int group_id2) {
		UnionFind::merge(group_id1, group_id2);
}

void CandidateUnionFind::updateScore(const Candidate *candidate) {
	// Update the top2 field if needed, starting at candidate and up.
	TopReverseTreeNode *node = dynamic_cast<TopReverseTreeNode *>(this->m_elements[candidate->getId()]);
	node->update();
	this->find(candidate);
}

bool CandidateUnionFind::isTopCandidate(const Candidate *candidate) {
	unsigned int group = this->find(candidate);
	TopReverseTreeNode *node = dynamic_cast<TopReverseTreeNode *>(this->m_groups[group]);
	return (node->getFirst() == candidate);
}

const Candidate *CandidateUnionFind::getLeader(const Candidate *candidate) {
	unsigned int group = this->find(candidate);
	TopReverseTreeNode *node = dynamic_cast<TopReverseTreeNode *>(this->m_groups[group]);
	return node->getFirst();
}

ReverseTreeNode *CandidateUnionFind::allocTreeNode(unsigned int id) const {
	return new TopReverseTreeNode(id);
}

/*
 * AVLTree.hpp
 *
 *  Created on: Apr 7, 2014
 *      Author: dkuznets
 */

#ifndef AVLTREE_HPP_
#define AVLTREE_HPP_

#include <stddef.h>
#include <exception>

#include "General.h"
#include "List.hpp"
#include "less.hpp"
#include "library2.h"

#define __MAX(a, b) ((a > b) ? a : b)

enum tree_step_t {
	ROOT, CHILD_LEFT, CHILD_RIGHT,
};

class AVLTreeException: public std::exception {
public:
	virtual const char *what() const throw () {
		return "AVLTree: general exception";
	}
};

class AVLTreeNoElementFound: public AVLTreeException {
public:
	virtual const char *what() const throw () {
		return "AVLTree: Element not found";
	}
};

class AVLTreeDuplicateElement: public AVLTreeException {
public:
	virtual const char *what() const throw () {
		return "AVLTree: Duplicate element detected";
	}
};

class AVLTreeUnbalancedError: public AVLTreeException {
public:
	virtual const char *what() const throw () {
		return "AVLTree: Unbalanced node found";
	}
};
template<typename T>
class AbstractSearchParameter {
public:
	virtual ~AbstractSearchParameter() {
	}
	// 0 -> equals,
	// neg -> T is less
	// pos -> T is more
	virtual int compare(T) const = 0;
};

template<typename T>
class AbstractWalkParameter {
public:
	virtual ~AbstractWalkParameter() {
	}

	virtual void apply(T &) = 0;
};

template<typename T, typename Compare = less<T> >
class AVLTree {

	class Node {
	private:
		T m_data;
		Node *m_parent;
		Node *m_left, *m_right;
		int m_balanceFactor;
		unsigned int m_height;
	public:
		Node(T data);

		T &getData() {
			return this->m_data;
		}

		Node *getLeft() const {
			return this->m_left;
		}

		Node *getRight() const {
			return this->m_right;
		}

		Node *getParent() const {
			return this->m_parent;
		}

		unsigned int getHeight() const {
			return this->m_height;
		}

		int getBalanceFactor() const {
			return this->m_balanceFactor;
		}

		bool isValid() const {
			return (this->m_balanceFactor > -2) && (this->m_balanceFactor < 2);
		}

		bool isUnbalanced() const {
			return (this->m_balanceFactor == -2) || (this->m_balanceFactor == 2);
		}

		void update();

		void setLeft(Node *left) {
			this->m_left = left;
		}

		void setRight(Node *right) {
			this->m_right = right;
		}

		void setParent(Node *parent) {
			this->m_parent = parent;
		}

		void setChild(tree_step_t which, Node *newChild) {
			if (which == CHILD_LEFT)
				this->m_left = newChild;
			else if (which == CHILD_RIGHT)
				this->m_right = newChild;
			else
				throw AVLTreeException();
		}

		Node *getChild(tree_step_t which) {
			if (which == CHILD_LEFT)
				return this->m_left;
			else if (which == CHILD_RIGHT)
				return this->m_right;
			else
				throw AVLTreeException();
		}
	};
public:
	AVLTree() :
			m_size(0), m_root(NULL) {

	}
	virtual ~AVLTree();

	void insert(T elem);

	void remove(T key);

	inline unsigned int size() const {
		return this->m_size;
	}

	T getNeighbor(T t) const;

	T findByParameter(const AbstractSearchParameter<T> &param) const;

	T findExactByParameter(const AbstractSearchParameter<T> &param) const;

	void reinitializeFromList(List<T> &list);

	void clear();

	void walkNodes(AbstractWalkParameter<T> &param);

#ifdef __TESTING
	void checkCorrectness() const;
	void checkNode(Node *node) const;
#endif // __TESTING

private:
	void rebalaceInsert(Node *from);

	void rebalanceRemove(Node *from);

	void rebalanceNode(Node *node);

	void destroyNodesPostOrd(Node *from);

	void rotateNodeLL(Node *node);
	void rotateNodeLRtoLL(Node *node);
	void rotateNodeRR(Node *node);
	void rotateNodeRLtoRR(Node *node);

	void walkNodesInOrder(Node *from, AbstractWalkParameter<T> &param);

	Node *createTreeFromListInOrd(size_t count,
			typename List<T>::Iterator &itr);

	unsigned int m_size;
	Node *m_root;
};

template<typename T, typename Compare>
AVLTree<T, Compare>::Node::Node(T data) {
	this->m_data = data;
	this->m_parent = NULL;
	this->m_left = NULL;
	this->m_right = NULL;
	this->m_height = 0;
	this->m_balanceFactor = 0;
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::Node::update() {
	if (this->getLeft() && this->getRight()) {
		this->m_height = 1
				+ __MAX(this->getLeft()->getHeight(),
						this->getRight()->getHeight());
		this->m_balanceFactor = this->getLeft()->getHeight()
				- this->getRight()->getHeight();
	} else if (this->getLeft()) {
		this->m_height = 1 + this->getLeft()->getHeight();
		this->m_balanceFactor = this->m_height;
	} else if (this->getRight()) {
		this->m_height = 1 + this->getRight()->getHeight();
		this->m_balanceFactor = 0 - this->m_height;
	} else {
		this->m_height = 0;
		this->m_balanceFactor = 0;
	}

	if (this->getParent())
		this->getParent()->update();
}

template<typename T, typename Compare>
inline AVLTree<T, Compare>::~AVLTree() {
	this->clear();
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::insert(T elem) {
	if (!this->size()) {
		this->m_root = new Node(elem);
		this->m_size++;
		return;
	}

	Node *prev = NULL;
	Node *current = this->m_root;
	tree_step_t last_step = ROOT;
	while (current) {
		prev = current;
		if (Compare::compare(elem, current->getData())) {
			current = current->getLeft();
			last_step = CHILD_LEFT;
		} else if (Compare::compare(current->getData(), elem)) {
			current = current->getRight();
			last_step = CHILD_RIGHT;
		} else {
			// Objects are equal, throw;
			throw AVLTreeDuplicateElement();
		}
	}
	Node *newNode = new Node(elem);
	newNode->setParent(prev);
	prev->setChild(last_step, newNode);
	prev->update();
	this->rebalaceInsert(newNode);
	this->m_size++;
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::remove(T elem) {
	if (!this->size()) {
		throw AVLTreeNoElementFound();
	}

	Node *current = this->m_root;
	tree_step_t last_step = ROOT;
	do {
		if (Compare::compare(elem, current->getData())) {
			current = current->getLeft();
			last_step = CHILD_LEFT;
		} else if (Compare::compare(current->getData(), elem)) {
			current = current->getRight();
			last_step = CHILD_RIGHT;
		} else {
			// Found the element!
			break;
		}
	} while (current);

	if (!current)
		throw AVLTreeNoElementFound();

	// Found the element!
	Node *parent = current->getParent();
	Node *left = current->getLeft();
	Node *right = current->getRight();
	Node *rebalanceFrom = NULL;

	if (left && right) {
		// Find largest node in left subtree:
		Node *maxLeft = left;
		while (maxLeft->getRight())
			maxLeft = maxLeft->getRight();

		// If maxLeft has a left sub tree, attach it to it's parent,
		// otherwise, attach nothing.
		if (maxLeft != left) {
			if (maxLeft->getLeft()) {
				maxLeft->getParent()->setRight(maxLeft->getLeft());
				maxLeft->getLeft()->setParent(maxLeft->getParent());
			} else {
				maxLeft->getParent()->setRight(NULL);
			}
			rebalanceFrom = maxLeft->getParent();
			maxLeft->getParent()->update();
			// Set original left subtree to maxLeft's left tree, original
			// right to maxLeft's right.
			maxLeft->setLeft(left);
			left->setParent(maxLeft);
		} else {
			rebalanceFrom = maxLeft;
		}

		maxLeft->setParent(parent);
		maxLeft->setRight(right);
		right->setParent(maxLeft);

		// Update root if needed
		if (current == this->m_root) {
			this->m_root = maxLeft;
		} else {
			parent->setChild(last_step, maxLeft);
		}

		maxLeft->update();
	} else if (left) {
		if (current == this->m_root) {
			this->m_root = left;
			left->setParent(NULL);
		} else {
			left->setParent(parent);
			parent->setChild(last_step, left);
			parent->update();
			rebalanceFrom = parent;
		}
	} else if (right) {
		if (current == this->m_root) {
			this->m_root = right;
			right->setParent(NULL);
		} else {
			right->setParent(parent);
			parent->setChild(last_step, right);
			parent->update();
			rebalanceFrom = parent;
		}
	} else {
		if (current == this->m_root) {
			this->m_root = NULL;
		} else {
			parent->setChild(last_step, NULL);
			parent->update();
			rebalanceFrom = parent;
		}
	}

	if (rebalanceFrom)
		this->rebalanceRemove(rebalanceFrom);

	delete current;
	this->m_size--;
}

// Try to find the correct element, if not found, try to find one that is to
// the left of the element (max of all smaller elements)
template<typename T, typename Compare>
T AVLTree<T, Compare>::findByParameter(
		const AbstractSearchParameter<T> &param) const {

	// Walk down the tree
	Node *prev = NULL;
	Node *current = this->m_root;
	while (current) {
		prev = current;
		int res = param.compare(current->getData());
		if (!res) {
			return current->getData();
		} else if (res < 0) {
			current = current->getLeft();
		} else {
			current = current->getRight();
		}
	}

	// Tree is empty
	if (!prev)
		throw AVLTreeNoElementFound();

	// If we under-shot, return:
	if (param.compare(prev->getData()) > 0)
		return prev->getData();

	// Otherwise, try to find the immediate node to the left: climb back up,
	// return the first node we're right child of:
	current = prev->getParent();
	while (current) {
		if (prev == current->getRight())
			return current->getData();

		prev = current;
		current = current->getParent();
	}

	throw AVLTreeNoElementFound();
}

template<typename T, typename Compare>
T AVLTree<T, Compare>::getNeighbor(T elem) const {
	Node *current = this->m_root;

	// Find target node
	while (current) {
		if (Compare::compare(elem, current->getData())) {
			current = current->getLeft();
		} else if (Compare::compare(current->getData(), elem)) {
			current = current->getRight();
		} else {
			break;
		}
	}

	if (!current) {
		throw AVLTreeNoElementFound();
	}

	// Try to get max from left first
	if (current->getLeft()) {
		Node *leftMax = current->getLeft();
		while (leftMax->getRight())
			leftMax = leftMax->getRight();

		return leftMax->getData();
		// Then min from right
	} else if (current->getRight()) {
		Node *rightMin = current->getRight();
		while (rightMin->getLeft())
			rightMin = rightMin->getLeft();
		return rightMin->getData();
		// Then parent
	} else if (current->getParent()) {
		return current->getParent()->getData();
	}

	// If there is no other nodes, no neighbor to return
	throw AVLTreeNoElementFound();
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::reinitializeFromList(List<T> &list) {
	this->clear();

	// Walk in order and graph from nodes
	typename List<T>::Iterator itr = list.begin();
	this->m_root = this->createTreeFromListInOrd((size_t) list.size(), itr);
	this->m_size = list.size();
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::destroyNodesPostOrd(Node* from) {
	if (!from)
		return;
	if (from->getLeft()) {
		this->destroyNodesPostOrd(from->getLeft());
		from->setLeft(NULL);
	}

	if (from->getRight()) {
		this->destroyNodesPostOrd(from->getRight());
		from->setRight(NULL);
	}

	delete from;
}

// Works for positive args only
static inline unsigned int __log2_ceil(unsigned int n) {
	unsigned int ret = 0;

	n >>= 1;

	while (n) {
		ret += 1;
		n >>= 1;
	}

	return ret;
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::clear() {
	// Post order walk, destroy all nodes
	this->destroyNodesPostOrd(this->m_root);
	this->m_root = NULL;
	this->m_size = 0;
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::walkNodes(AbstractWalkParameter<T> &param) {
	if (this->m_root) {
		this->walkNodesInOrder(this->m_root, param);
	}
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::walkNodesInOrder(Node* from,
		AbstractWalkParameter<T>& param) {
	if (from->getLeft()) {
		this->walkNodesInOrder(from->getLeft(), param);
	}
	param.apply(from->getData());

	if (from->getRight()) {
		this->walkNodesInOrder(from->getRight(), param);
	}
}

template<typename T, typename Compare>
typename AVLTree<T, Compare>::Node *
AVLTree<T, Compare>::createTreeFromListInOrd(size_t count,
		typename List<T>::Iterator &itr) {

	if (!count)
		return NULL;

	// Create left subtree
	Node *left = NULL;
	size_t left_size = 0;
	if (count > 1) {
		left_size = (1 << (__log2_ceil(count) - 1));
		left = this->createTreeFromListInOrd(left_size, itr);
	}
	Node *current = new Node(*itr);
	itr++;
	Node *right = this->createTreeFromListInOrd(count - 1 - left_size, itr);

	current->setLeft(left);
	if (left)
		left->setParent(current);
	current->setRight(right);
	if (right)
		right->setParent(current);
	current->update();

	return current;
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rebalaceInsert(Node* from) {
	while (from && from->isValid())
		from = from->getParent();
	if (from)
		this->rebalanceNode(from);
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rebalanceRemove(Node* from) {
	while (from) {
		if (!from->isValid())
			this->rebalanceNode(from);
		from = from->getParent();
	}
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rebalanceNode(Node* node) {
	if (node->isValid())
		return;
	if (!node->isUnbalanced())
		throw AVLTreeUnbalancedError();

	switch (node->getBalanceFactor()) {
	case -2:
		// rotateRx
		switch (node->getRight()->getBalanceFactor()) {
		// rotateRL
		case 1:
			this->rotateNodeRLtoRR(node);
			// rotateRR
		case -1:
		case 0:
			this->rotateNodeRR(node);
			break;
		default:
			throw AVLTreeUnbalancedError();
		}
		break;
	case 2:
		// rotateLx
		switch (node->getLeft()->getBalanceFactor()) {
		// rotateLR
		case -1:
			this->rotateNodeLRtoLL(node);
			// rotateLL
		case 0:
		case 1:
			this->rotateNodeLL(node);
			break;
		default:
			throw AVLTreeUnbalancedError();
		}
		break;
	}
}

#ifdef __TESTING
template<typename T, typename Compare>
void AVLTree<T, Compare>::checkCorrectness() const {
#if 0
	this->checkNode(this->m_root);
#endif
}

template<typename T, typename Compare>
void AVLTree<T, Compare>::checkNode(Node *node) const {
	if (!node) {
		return;
	}

	if (!node->isValid()) {
		throw AVLTreeUnbalancedError();
	}

	if (node->getLeft()) {
		T curr = node->getData();
		T left = node->getLeft()->getData();

		if (left >= curr || node == node->getLeft()) {
			throw AVLTreeException();
		}
		this->checkNode(node->getLeft());
	}
	if (node->getRight()) {
		T curr = node->getData();
		T right = node->getRight()->getData();

		if (right <= curr || node == node->getRight()) {
			throw AVLTreeException();
		}
		this->checkNode(node->getRight());
	}
}

#endif // __TESTING

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rotateNodeLL(Node *node) {
	Node *left = node->getLeft();
	//Node *leftLeft = left->getLeft();
	Node *leftRight = left->getRight();
	Node *parent = node->getParent();

	if (leftRight) {
		node->setLeft(leftRight);
		leftRight->setParent(node);
	} else {
		node->setLeft(NULL);
	}

	left->setRight(node);
	node->setParent(left);

	if (parent) {
		left->setParent(parent);
		if (parent->getLeft() == node)
			parent->setLeft(left);
		else
			parent->setRight(left);
	} else {
		this->m_root = left;
		left->setParent(NULL);
	}

	node->update();
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rotateNodeLRtoLL(Node *node) {
	Node *left = node->getLeft();
	Node *leftRight = left->getRight();
	Node *leftRightLeft = leftRight->getLeft();

	node->setLeft(leftRight);
	leftRight->setParent(node);

	leftRight->setLeft(left);
	left->setParent(leftRight);

	if (leftRightLeft) {
		left->setRight(leftRightLeft);
		leftRightLeft->setParent(left);
	} else {
		left->setRight(NULL);
	}

	left->update();
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rotateNodeRR(Node *node) {
	Node *right = node->getRight();
	//Node *rightRight = right->getRight();
	Node *rightLeft = right->getLeft();
	Node *parent = node->getParent();

	if (rightLeft) {
		node->setRight(rightLeft);
		rightLeft->setParent(node);
	} else {
		node->setRight(NULL);
	}

	right->setLeft(node);
	node->setParent(right);

	if (parent) {
		right->setParent(parent);
		if (parent->getLeft() == node)
			parent->setLeft(right);
		else
			parent->setRight(right);
	} else {
		this->m_root = right;
		right->setParent(NULL);
	}
	node->update();
}

template<typename T, typename Compare>
inline T AVLTree<T, Compare>::findExactByParameter(
		const AbstractSearchParameter<T>& param) const {
	T res = this->findByParameter(param);
	if (param.compare(res))
		throw AVLTreeNoElementFound();
	return res;
}

template<typename T, typename Compare>
inline void AVLTree<T, Compare>::rotateNodeRLtoRR(Node *node) {
	Node *right = node->getRight();
	Node *rightLeft = right->getLeft();
	Node *rightLeftRight = rightLeft->getRight();

	node->setRight(rightLeft);
	rightLeft->setParent(node);

	rightLeft->setRight(right);
	right->setParent(rightLeft);

	if (rightLeftRight) {
		right->setLeft(rightLeftRight);
		rightLeftRight->setParent(right);
	} else {
		right->setLeft(NULL);
	}

	right->update();
}

#endif /* AVLTREE_HPP_ */

#ifndef HASH_HPP_
#define HASH_HPP_

#include <cstddef>
#include <exception>
#include "List.hpp"
#include "Pair.hpp"

class IHashable {
public:
	virtual ~IHashable() {}
	virtual unsigned int hash() const = 0;
};

class IComparable {
public:
	virtual ~IComparable() {}
	virtual bool compareTo(const IComparable &other) const = 0;
};


class AbstractKeyType : public IHashable, public IComparable {
public:
	virtual ~AbstractKeyType() {}
};


class HashException : public std::exception { };
class HashKeyNotPresentException : public HashException { };

template <typename TKeyType, typename TValueType>
class Hash {
public:
	Hash(size_t max_size);
	virtual ~Hash();

	size_t size() const;
	size_t max_size() const;

	bool hasKey(const TKeyType &key) const;
	const TValueType &getKey(const TKeyType &key) const;

	void addKey(const TKeyType &key, const TValueType &value);
	void removeKey(const TKeyType &key);

protected:
	size_t m_max_size;
	size_t m_size;

	List<Pair<TKeyType, TValueType> > **m_hash;
};

template <typename TKeyType, typename TValueType>
Hash<TKeyType, TValueType>::Hash(size_t max_size) : m_max_size(max_size), m_size(0) {
	this->m_hash = new List<Pair<TKeyType, TValueType> >*[this->m_max_size];
	for (unsigned int i = 0; i < this->m_max_size; i++) {
		this->m_hash[i] = NULL;
	}
}

template <typename TKeyType, typename TValueType>
Hash<TKeyType, TValueType>::~Hash() {
	for (unsigned int i = 0; i < this->m_max_size; i++) {
		if (this->m_hash[i] != NULL) {
			delete this->m_hash[i];
		}
	}

	delete [] this->m_hash;
}

template <typename TKeyType, typename TValueType>
size_t Hash<TKeyType, TValueType>::size() const {
	return this->m_size;
}

template <typename TKeyType, typename TValueType>
size_t Hash<TKeyType, TValueType>::max_size() const {
	return this->m_max_size;
}

template <typename TKeyType, typename TValueType>
const TValueType &Hash<TKeyType, TValueType>::getKey(const TKeyType &key) const {
	List<Pair<TKeyType, TValueType> > *list = this->m_hash[key.hash() % this->m_max_size];
	if (list == NULL) {
		throw HashKeyNotPresentException();
	}

	typename List<Pair<TKeyType, TValueType> >::Iterator itr = list->begin();
	while (itr.inBounds()) {
		if (key.compareTo((*itr).first)) {
			return (*itr).second;
		}

		itr++;
	}

	throw HashKeyNotPresentException();
}

template <typename TKeyType, typename TValueType>
bool Hash<TKeyType, TValueType>::hasKey(const TKeyType &key) const {
	try {
		this->getKey(key);
		return true;
	} catch (HashKeyNotPresentException &e) {
		return false;
	}
}

template <typename TKeyType, typename TValueType>
void Hash<TKeyType, TValueType>::addKey(const TKeyType &key, const TValueType &value) {
	List<Pair<TKeyType, TValueType> > *list = this->m_hash[key.hash() % this->m_max_size];

	if (list != NULL) {
		typename List<Pair<TKeyType, TValueType> >::Iterator itr = list->begin();
		while (itr.inBounds()) {
			if (key.compareTo((*itr).first)) {
				(*itr).second = value;
				return;
			}
			itr++;
		}
	} else {
		list = new List<Pair<TKeyType, TValueType> >();
		this->m_hash[key.hash() % this->m_max_size] = list;
	}

	list->append(Pair<TKeyType, TValueType>(key, value));
	this->m_size++;
}

template <typename TKeyType, typename TValueType>
void Hash<TKeyType, TValueType>::removeKey(const TKeyType &key) {
	if (!this->hasKey(key)) {
		throw HashKeyNotPresentException();
	}

	List<Pair<TKeyType, TValueType> > *list = this->m_hash[key.hash() % this->m_max_size];
	if (list) {
		typename List<Pair<TKeyType, TValueType> >::Iterator itr = list->begin();
		while (itr.inBounds()) {
			if (key.compareTo((*itr).first)) {
				list->removeAt(itr);
				this->m_size--;
				return;
			}
		}
	}
}

#endif /* HASH_HPP_ */
